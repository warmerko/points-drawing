﻿using VContainer;
using VContainer.Unity;

namespace PointDrawing.Gameplay
{
    public class ConfigsLoadingLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<ConfigsLoading>();
        }
    }
}
