﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PointDrawing.Gameplay.Configs
{
    [CreateAssetMenu(fileName = "BackgroundsConfig", menuName = "PD/Configs/BackgroundsConfig", order = 0)]
    public class BackgroundsConfig : ScriptableObject
    {
        [SerializeField] private AssetReference[] _backgrounds;
        [SerializeField] private float _screenMargin;
        [SerializeField] private float _itemFillRate;


        public AssetReference[] Backgrounds => _backgrounds;
        public float ScreenMargin => _screenMargin;
        public float ItemFillRate => _itemFillRate;
    }
}
