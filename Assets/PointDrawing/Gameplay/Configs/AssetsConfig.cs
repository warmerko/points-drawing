﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PointDrawing.Gameplay.Configs
{
    [CreateAssetMenu(fileName = "AssetsConfig", menuName = "PD/Configs/AssetsConfig", order = 0)]
    public class AssetsConfig : ScriptableObject
    {
        [SerializeField] private AssetReference _backgroundsConfigRef;
        [SerializeField] private AssetReference _itemsConfigRef;
        [SerializeField] private AssetReference _gameplayConfigRef;

        public AssetReference BackgroundConfigRef => _backgroundsConfigRef;
        public AssetReference ItemsConfigRef => _itemsConfigRef;
        public AssetReference GameplayConfigRef => _gameplayConfigRef;
    }
}
