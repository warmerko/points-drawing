﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PointDrawing.Gameplay.Configs
{
    [CreateAssetMenu(fileName = "AppConfig", menuName = "PD/Configs/AppConfig", order = 0)]
    public class GameplayConfig : ScriptableObject
    {
        [SerializeField] private AssetReference _activeLineRef;
        [SerializeField] private AssetReference _completedLineRef;
        [SerializeField] private int _itemsInOneGame;
        [SerializeField] private int _showItemMs;
        [SerializeField] private float _itemFillRate;

        public AssetReference ActiveLineRef => _activeLineRef;
        public AssetReference CompletedLineRef => _completedLineRef;

        public int ItemsInOneGame => _itemsInOneGame;
        public int ShowItemMs => _showItemMs;
        public float ItemFillRate => _itemFillRate;
    }
}
