﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PointDrawing.Gameplay.Configs
{
    [CreateAssetMenu(fileName = "ItemsConfig", menuName = "PD/Configs/ItemsConfig", order = 0)]
    public class ItemsConfig : ScriptableObject
    {
        [SerializeField] private AssetReference _point;
        [SerializeField] private AssetReference[] _items;

        public AssetReference[] Items => _items;
        public AssetReference Point => _point;
    }
}
