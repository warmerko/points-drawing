﻿using System.Threading;
using Cysharp.Threading.Tasks;
using PointDrawing.Common.Utils;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Factories;
using VContainer;
using VContainer.Unity;

namespace PointDrawing.Gameplay
{
    public class ConfigsLoading : IAsyncStartable
    {
        private readonly LifetimeScope _currentScope;
        private readonly AssetsConfig _assetsConfig;

        protected ConfigsLoading(AssetsConfig assetsConfig, LifetimeScope lifetimeScope)
        {
            _assetsConfig = assetsConfig;
            _currentScope = lifetimeScope;
        }

        public async UniTask StartAsync(CancellationToken _)
        {
            var gameplayConfig = await AddressablesUtils.LoadAssetAsync<GameplayConfig>(_assetsConfig.GameplayConfigRef);
            var backgroundsConfig = await AddressablesUtils.LoadAssetAsync<BackgroundsConfig>(_assetsConfig.BackgroundConfigRef);
            var itemsConfig = await AddressablesUtils.LoadAssetAsync<ItemsConfig>(_assetsConfig.ItemsConfigRef);

            var loadingScope = _currentScope.CreateChild(builder =>
            {
                builder.RegisterEntryPoint<AssetsLoadingAndGameplayLoop>();

                builder.Register<BackgroundFactory>(Lifetime.Singleton);
                builder.Register<ItemsFactory>(Lifetime.Singleton);
                builder.Register<PointsFactory>(Lifetime.Singleton);

                builder.RegisterInstance(gameplayConfig);
                builder.RegisterInstance(backgroundsConfig);
                builder.RegisterInstance(itemsConfig);
            });
            loadingScope.name = "AssetsLoadingLifetimeScope";
        }
    }
}
