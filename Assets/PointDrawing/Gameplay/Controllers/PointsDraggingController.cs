﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using UnityEngine.EventSystems;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class PointsDraggingController : IAsyncStartable
    {
        private readonly PointView[] _pointViews;
        private readonly PointsRepository _pointsRepository;

        private PointView _activePointView;
        private PointView _nextPointView;

        protected PointsDraggingController(PointView[] pointViews, PointsRepository pointsRepository)
        {
            _pointViews = pointViews;
            _pointsRepository = pointsRepository;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            while (token.IsCancellationRequested == false)
            {
                await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible == false, cancellationToken: token);

                using (_pointsRepository.ActivePointIndex.Subscribe(ChangeActivePoint))
                using (_pointsRepository.NextPointIndex.Subscribe(ChangeNextPoint))
                {
                    await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible, cancellationToken: token);
                }
            }
        }

        private void ChangeActivePoint(int pointIndex)
        {
            if (_activePointView != null)
            {
                _activePointView.OnBeginDragEvent -= OnBeginDragEventHandler;
                _activePointView.OnDragEvent -= OnDragEventHandler;
                _activePointView.OnEndDragEvent -= OnEndDragEventHandler;
            }

            _activePointView = _pointViews[pointIndex];

            _activePointView.OnBeginDragEvent += OnBeginDragEventHandler;
            _activePointView.OnDragEvent += OnDragEventHandler;
            _activePointView.OnEndDragEvent += OnEndDragEventHandler;
        }

        private void ChangeNextPoint(int nextPointIndex)
        {
            _nextPointView = _pointViews[nextPointIndex];
        }

        private void OnEndDragEventHandler()
        {
            _pointsRepository.SetActivePointDragging(false);
        }

        private void OnDragEventHandler(Vector3 position)
        {
            if (Vector3.Magnitude(_nextPointView.transform.position - position) > _nextPointView.Collider.radius) return;

            var pointerEventData = _activePointView.PointerEventData;
            var nextPointViewGameObject = _nextPointView.gameObject;

            ExecuteEvents.Execute(_activePointView.gameObject, pointerEventData, ExecuteEvents.endDragHandler);

            _pointsRepository.SetNextActivePoint();

            pointerEventData.pointerDrag = nextPointViewGameObject;

            ExecuteEvents.Execute(nextPointViewGameObject, pointerEventData, ExecuteEvents.beginDragHandler);
        }

        private void OnBeginDragEventHandler()
        {
            _pointsRepository.SetActivePointDragging(true);
        }
    }
}
