﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class ActiveLineController : IAsyncStartable
    {
        private readonly PointsRepository _pointsRepository;
        private readonly ItemsRepository _itemsRepository;
        private readonly PointView[] _pointViews;

        private readonly ActiveLineView _lineView;

        private Vector2 _itemBoundsCenter;

        protected ActiveLineController(PointsRepository pointsRepository, ItemsRepository itemsRepository, PointView[] pointViews, ActiveLineView lineView)
        {
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;

            _pointViews = pointViews;
            _lineView = lineView;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            while (token.IsCancellationRequested == false)
            {
                await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible == false, cancellationToken: token);

                var item = _itemsRepository.CurrentItem.Value;
                _itemBoundsCenter = new Vector2(item.Bounds.center.x, item.Bounds.center.y);

                using (_pointsRepository.IsPointDragging.Subscribe(IsPointDraggingHandler))
                {
                    await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible, cancellationToken: token);
                }
            }
        }

        private void IsPointDraggingHandler(bool isDragging)
        {
            if (isDragging)
            {
                _lineView.gameObject.SetActive(true);

                var item = _itemsRepository.CurrentItem.Value;
                var pointPosition = item.Points[_pointsRepository.ActivePointIndex.Value];
                var pointView = _pointViews[_pointsRepository.ActivePointIndex.Value];

                _lineView.SetFirstPosition(pointPosition - _itemBoundsCenter);

                pointView.OnDragEvent += OnDragEventHandler;
            }
            else
            {
                _lineView.gameObject.SetActive(false);

                var activePointView = _pointViews[_pointsRepository.ActivePointIndex.Value];

                activePointView.OnDragEvent -= OnDragEventHandler;
            }
        }

        private void OnDragEventHandler(Vector3 position)
        {
            if (Mathf.Approximately(position.sqrMagnitude, 0f) == false)
            {
                _lineView.SetLastPosition(position);
            }
        }
    }
}
