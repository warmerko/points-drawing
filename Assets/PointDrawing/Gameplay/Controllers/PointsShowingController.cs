﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class PointsShowingController : IAsyncStartable
    {
        private readonly PointView[] _pointViews;
        private readonly ItemsRepository _itemsRepository;
        private readonly PointsRepository _pointsRepository;

        private IDisposable _subscription;

        protected PointsShowingController(PointView[] pointViews, ItemsRepository itemsRepository, PointsRepository pointsRepository)
        {
            _pointViews = pointViews;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            while (token.IsCancellationRequested == false)
            {
                await ShowCurrentItemPointsAsync(_itemsRepository.CurrentItem.Value);

                _pointsRepository.IsPointsVisible = true;

                await _itemsRepository.CurrentItem.WaitAsync(token);
            }
        }

        private async UniTask ShowCurrentItemPointsAsync(Item item)
        {
            _subscription?.Dispose();

            await HidePointsAsync();

            _subscription = _itemsRepository.CurrentItem.Value.IsComplete.Skip(1).SubscribeAwait(HidePointsAsync);

            foreach (var pointView in _pointViews)
            {
                pointView.gameObject.SetActive(false);
            }

            var boundsCenter = new Vector2(item.Bounds.center.x, item.Bounds.center.y);

            for (var i = 0; i < item.Points.Length; ++i)
            {
                var pointPosition = item.Points[i];
                var pointView = _pointViews[i];
                var pointTransform = pointView.transform;

                pointView.ResetView();
                pointView.gameObject.SetActive(true);
                pointView.SetAlpha(0f);

                pointPosition -= boundsCenter;

                pointTransform.position = new Vector3(pointPosition.x, pointPosition.y, -0.1f);

                await pointView.DoIn();
            }
        }

        private UniTask HidePointsAsync(bool _ = false)
        {
            var fadeOuts = new List<UniTask>();

            foreach (var pointView in _pointViews)
            {
                if (pointView.gameObject.activeInHierarchy)
                {
                    fadeOuts.Add(pointView.DoFade(0f).ToUniTask());
                }
            }

            return UniTask.WhenAll(fadeOuts);
        }
    }
}
