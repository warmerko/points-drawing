﻿using System.Threading;
using Cysharp.Threading.Tasks;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class BackgroundController : IAsyncStartable
    {
        private readonly BackgroundsConfig _backgroundsConfig;
        private readonly Camera _camera;
        private readonly BackgroundView _backgroundView;

        public BackgroundController(BackgroundsConfig backgroundsConfig, Camera camera, BackgroundView backgroundView)
        {
            _backgroundsConfig = backgroundsConfig;
            _camera = camera;
            _backgroundView = backgroundView;
        }

        public UniTask StartAsync(CancellationToken cancellation)
        {


            return UniTask.CompletedTask;
        }
    }
}
