﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class CompletedLineController : IAsyncStartable
    {
        private readonly PointsRepository _pointsRepository;
        private readonly ItemsRepository _itemsRepository;
        private readonly CompletedLineView _completedLineView;

        protected CompletedLineController(PointsRepository pointsRepository, ItemsRepository itemsRepository, CompletedLineView completedLineView)
        {
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _completedLineView = completedLineView;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            while (token.IsCancellationRequested == false)
            {
                await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible == false, cancellationToken: token);

                _completedLineView.gameObject.SetActive(true);
                _completedLineView.SetAlpha(1f);

                using (_pointsRepository.ActivePointIndex.Subscribe(ActivePointIndexChangedHandler))
                {
                    var item = _itemsRepository.CurrentItem.Value;
                    await UniTask.WaitWhile(() => item.IsComplete.Value == false, cancellationToken: token);

                    ActivePointIndexChangedHandler(0);

                    await _completedLineView.DoFade(0f, 0.4f);

                    _completedLineView.gameObject.SetActive(false);
                    _completedLineView.Clear();

                    await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible, cancellationToken: token);
                }
            }
        }

        private void ActivePointIndexChangedHandler(int index)
        {
            var activeItem = _itemsRepository.CurrentItem.Value;
            var activePointPosition = activeItem.Points[index];
            var boundsCenter = new Vector2(activeItem.Bounds.center.x, activeItem.Bounds.center.y);

            _completedLineView.AddPosition(activePointPosition - boundsCenter);
        }
    }
}
