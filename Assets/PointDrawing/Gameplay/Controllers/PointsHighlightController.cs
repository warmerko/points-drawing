﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class PointsHighlightController : IAsyncStartable
    {
        private readonly PointsRepository _pointsRepository;
        private readonly PointView[] _pointViews;

        private PointView _activePointView;

        protected PointsHighlightController(PointsRepository pointsRepository, PointView[] pointViews)
        {
            _pointsRepository = pointsRepository;
            _pointViews = pointViews;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            while (token.IsCancellationRequested == false)
            {
                await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible == false, cancellationToken: token);

                using (_pointsRepository.ActivePointIndex.Subscribe(ChangeActivePoint))
                using (_pointsRepository.NextPointIndex.Subscribe(ChangeNextPoint))
                using (_pointsRepository.IsPointDragging.Subscribe(SetActivePointDragging))
                {
                    await UniTask.WaitWhile(() => _pointsRepository.IsPointsVisible, cancellationToken: token);
                }
            }
        }

        private void ChangeActivePoint(int index)
        {
            var prevPointExists = _activePointView != null;

            if (prevPointExists)
            {
                _activePointView.SetCompleted();
            }

            _activePointView = _pointViews[index];

            _activePointView.SetHighlight();

            if (prevPointExists && index != 0)
            {
                _activePointView.ShowParticlesAsync().Forget();
            }
        }

        private void ChangeNextPoint(int index)
        {
            var nextPointView = _pointViews[index];

            nextPointView.SetNext();
        }

        private void SetActivePointDragging(bool isDragging)
        {
            if (isDragging)
            {
                _activePointView.SetCompleted();
            }
            else
            {
                _activePointView.SetHighlight();
            }
        }
    }
}
