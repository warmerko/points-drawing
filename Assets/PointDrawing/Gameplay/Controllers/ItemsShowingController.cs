﻿using System.Threading;
using Cysharp.Threading.Tasks;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Controllers
{
    public class ItemsShowingController : IAsyncStartable
    {
        private readonly ItemView[] _itemViews;

        private readonly ItemsRepository _itemsRepository;

        private readonly GameplayConfig _gameplayConfig;

        private readonly BackgroundView _backgroundView;
        private readonly BackgroundsConfig _backgroundsConfig;

        public ItemsShowingController(ItemView[] itemViews, ItemsRepository itemsRepository, GameplayConfig gameplayConfig,
            BackgroundView backgroundView, BackgroundsConfig backgroundsConfig)
        {
            _itemViews = itemViews;

            _itemsRepository = itemsRepository;

            _gameplayConfig = gameplayConfig;

            _backgroundView = backgroundView;
            _backgroundsConfig = backgroundsConfig;
        }

        private async UniTask ShowItemAsync(int currentItemIndex)
        {
            var itemView = _itemViews[currentItemIndex];

            itemView.gameObject.SetActive(true);

            var itemBounds = itemView.Bounds;
            var itemBoundsSize = itemBounds.size;
            var itemTransform = itemView.transform;

            float factor;

            if (itemBoundsSize.x >= itemBoundsSize.y)
            {
                factor = _backgroundView.BoardSize.x * _backgroundsConfig.ItemFillRate / itemBoundsSize.x;
            }
            else
            {
                factor = _backgroundView.BoardSize.y * _backgroundsConfig.ItemFillRate / itemBoundsSize.y;
            }

            itemTransform.localScale = new Vector3(factor, factor, factor);

            var center = new Vector3(itemBounds.center.x * factor, itemBounds.center.y * factor, itemBounds.center.z * factor);
            var positionOffset = itemTransform.position - center;

            itemTransform.Translate(positionOffset);

            itemView.SetAlpha(0f);

            await itemView.DoFade(1f);

            await UniTask.Delay(_gameplayConfig.ShowItemMs);

            await itemView.DoFade(0f);

            itemView.gameObject.SetActive(false);
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            do
            {
                var currentItemIndex = _itemsRepository.CurrentItemIndex.Value;
                var currentItem = _itemsRepository.CurrentItem.Value;

                await currentItem.IsComplete.WaitAsync(token);

                await ShowItemAsync(currentItemIndex);

                _itemsRepository.SetNextItem();
            } while (_itemsRepository.IsEndOfItems == false);
        }
    }
}
