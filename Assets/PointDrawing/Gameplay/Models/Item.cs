﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace PointDrawing.Gameplay.Models
{
    public class Item
    {
        private readonly Vector2[] _points;
        private readonly Bounds _bounds;
        private AsyncReactiveProperty<bool> _isComplete;

        public Vector2[] Points => _points;
        public Bounds Bounds => _bounds;

        public IReadOnlyAsyncReactiveProperty<bool> IsComplete => _isComplete;

        private Item(Vector2[] points, Bounds bounds)
        {
            _points = points;
            _bounds = bounds;

            _isComplete = new AsyncReactiveProperty<bool>(false);
        }

        public void Complete()
        {
            _isComplete.Value = true;
        }

        public static Item Create(Vector2[] points, Bounds bounds, Vector3 boardSize, float itemFillRate)
        {
            var scaledPoints = new Vector2[points.Length];

            float scale;

            if (bounds.size.x >= bounds.size.y)
            {
                scale = boardSize.x * itemFillRate / bounds.size.x;
            }
            else
            {
                scale = boardSize.y * itemFillRate / bounds.size.y;
            }

            var scaledBounds = new Bounds(bounds.center * scale, bounds.size * scale);

            for (var i = 0; i < scaledPoints.Length; ++i)
            {
                scaledPoints[i] = points[i] * scale;
            }

            return new Item(scaledPoints, scaledBounds);
        }
    }
}
