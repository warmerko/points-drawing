﻿using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using VContainer.Unity;

namespace PointDrawing.Gameplay.Models
{
    public class PointsRepository : IStartable
    {
        private readonly ItemsRepository _itemsRepository;

        private readonly AsyncReactiveProperty<int> _activePointIndex;
        private readonly AsyncReactiveProperty<int> _nextPointIndex;

        private readonly AsyncReactiveProperty<bool> _isPointDragging;

        public IReadOnlyAsyncReactiveProperty<int> ActivePointIndex => _activePointIndex;
        public IReadOnlyAsyncReactiveProperty<int> NextPointIndex => _nextPointIndex;

        public IReadOnlyAsyncReactiveProperty<bool> IsPointDragging => _isPointDragging;
        public bool IsPointsVisible { get; set; }

        protected PointsRepository(ItemsRepository itemsRepository)
        {
            _itemsRepository = itemsRepository;

            _activePointIndex = new AsyncReactiveProperty<int>(0);
            _nextPointIndex = new AsyncReactiveProperty<int>(1);

            _isPointDragging = new AsyncReactiveProperty<bool>(false);
        }

        public void Start()
        {
            _itemsRepository.CurrentItem.Skip(1).Subscribe(Reset);
        }

        private void Reset(Item _)
        {
            _activePointIndex.Value = 0;
            _nextPointIndex.Value = 1;

            _isPointDragging.Value = false;

            IsPointsVisible = false;
        }

        public void SetNextActivePoint()
        {
            if (_nextPointIndex.Value == 0)
            {
                _itemsRepository.CurrentItem.Value.Complete();
                return;
            }

            _activePointIndex.Value += 1;
            _nextPointIndex.Value = (_activePointIndex.Value + 1) % _itemsRepository.CurrentItem.Value.Points.Length;
        }

        public void SetActivePointDragging(bool isDragging)
        {
            _isPointDragging.Value = isDragging;
        }
    }
}
