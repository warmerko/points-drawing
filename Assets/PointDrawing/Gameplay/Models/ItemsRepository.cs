﻿using Cysharp.Threading.Tasks;
using PointDrawing.Gameplay.Configs;
using UnityEngine.Assertions;

namespace PointDrawing.Gameplay.Models
{
    public class ItemsRepository
    {
        private readonly GameplayConfig _gameplayConfig;
        private readonly Item[] _items;

        private readonly AsyncReactiveProperty<int> _currentItemIndex;
        private readonly AsyncReactiveProperty<Item> _currentItem;

        public IReadOnlyAsyncReactiveProperty<int> CurrentItemIndex => _currentItemIndex;
        public IReadOnlyAsyncReactiveProperty<Item> CurrentItem => _currentItem;

        public bool IsEndOfItems => _currentItemIndex == _gameplayConfig.ItemsInOneGame;

        public ItemsRepository(Item[] items, GameplayConfig gameplayConfig)
        {
            Assert.IsTrue(items.Length == gameplayConfig.ItemsInOneGame);
            Assert.IsTrue(items.Length > 0);

            _items = items;

            _gameplayConfig = gameplayConfig;

            _currentItemIndex = new AsyncReactiveProperty<int>(0);
            _currentItem = new AsyncReactiveProperty<Item>(_items[0]);
        }

        public void SetNextItem()
        {
            _currentItemIndex.Value += 1;

            if (IsEndOfItems) return;

            _currentItem.Value = _items[_currentItemIndex.Value];
        }
    }
}
