﻿using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using PointDrawing.Common.Utils;
using PointDrawing.Common.Views;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Controllers;
using PointDrawing.Gameplay.Factories;
using PointDrawing.Gameplay.Models;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using UnityEngine.AddressableAssets;
using VContainer;
using VContainer.Unity;

namespace PointDrawing.Gameplay
{
    public class AssetsLoadingAndGameplayLoop : IAsyncStartable
    {
        private readonly BackgroundFactory _backgroundFactory;
        private readonly ItemsFactory _itemsFactory;
        private readonly PointsFactory _pointsFactory;

        private readonly GameplayConfig _gameplayConfig;
        private readonly BackgroundsConfig _backgroundsConfig;

        private readonly LoadingScreenView _loadingScreenView;

        private readonly LifetimeScope _currentScope;

        protected AssetsLoadingAndGameplayLoop(BackgroundFactory backgroundFactory, ItemsFactory itemsFactory, PointsFactory pointsFactory, GameplayConfig gameplayConfig,
            BackgroundsConfig backgroundsConfig, LoadingScreenView loadingScreenView, LifetimeScope currentScope)
        {
            _backgroundFactory = backgroundFactory;
            _itemsFactory = itemsFactory;
            _pointsFactory = pointsFactory;

            _gameplayConfig = gameplayConfig;
            _backgroundsConfig = backgroundsConfig;

            _loadingScreenView = loadingScreenView;

            _currentScope = currentScope;
        }

        public async UniTask StartAsync(CancellationToken token)
        {
            var camera = Camera.main;

            while (token.IsCancellationRequested == false)
            {
                _loadingScreenView.RestartButton.gameObject.SetActive(false);
                _loadingScreenView.SetLoadingVisible(true);

                var backgroundView = await _backgroundFactory.CreateRandomAsync();
                var itemViews = await _itemsFactory.CreateRandomAsync(_gameplayConfig.ItemsInOneGame);
                var pointViews = await _pointsFactory.CreateAsync(itemViews.Max(i => i.Points.Length));
                var activeLineView = await AddressablesUtils.InstantiateAsync<ActiveLineView>(_gameplayConfig.ActiveLineRef);
                var completedLineView = await AddressablesUtils.InstantiateAsync<CompletedLineView>(_gameplayConfig.CompletedLineRef);

                var worldSize = ScreenUtils.GetScreenToWorldSize(camera);
                var factor = worldSize.y / (backgroundView.BoardSize.y + _backgroundsConfig.ScreenMargin * 2f);

                backgroundView.transform.localScale = new Vector3(factor, factor, factor);

                var gameplayScope = _currentScope.CreateChild(builder =>
                {
                    var boardSize = backgroundView.BoardSize * factor;

                    builder.Register<ItemsRepository>(Lifetime.Singleton)
                        .WithParameter(itemViews.Select(i => Item.Create(i.Points, i.Bounds, boardSize, _gameplayConfig.ItemFillRate)).ToArray())
                        .AsSelf();

                    builder.Register<PointsRepository>(Lifetime.Singleton)
                        .AsImplementedInterfaces()
                        .AsSelf();

                    builder.RegisterInstance(backgroundView);
                    builder.RegisterInstance(itemViews);
                    builder.RegisterInstance(pointViews);
                    builder.RegisterInstance(activeLineView);
                    builder.RegisterInstance(completedLineView);

                    builder.RegisterInstance(camera);

                    builder.Register<BackgroundController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.Register<PointsShowingController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.Register<PointsDraggingController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.Register<PointsHighlightController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.Register<ActiveLineController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.Register<CompletedLineController>(Lifetime.Singleton).AsImplementedInterfaces();
                    builder.RegisterEntryPoint<ItemsShowingController>();
                });
                gameplayScope.name = "GameplayLifetimeScope";

                _loadingScreenView.SetLoadingVisible(false);

                await _loadingScreenView.DoFade(0f);

                var itemsRepository = gameplayScope.Container.Resolve<ItemsRepository>();

                await UniTask.WaitWhile(() => itemsRepository.IsEndOfItems == false, cancellationToken: token);

                await _loadingScreenView.DoFade(1f);

                Addressables.ReleaseInstance(completedLineView.gameObject);
                Addressables.ReleaseInstance(activeLineView.gameObject);

                foreach (var pointView in pointViews)
                    Addressables.ReleaseInstance(pointView.gameObject);

                foreach (var itemView in itemViews)
                    Addressables.ReleaseInstance(itemView.gameObject);

                Addressables.ReleaseInstance(backgroundView.gameObject);

                gameplayScope.Dispose();

                _loadingScreenView.RestartButton.gameObject.SetActive(true);
                await _loadingScreenView.RestartButton.OnClickAsync(token);
            }
        }
    }
}
