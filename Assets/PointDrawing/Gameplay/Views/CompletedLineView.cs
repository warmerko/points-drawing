﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace PointDrawing.Gameplay.Views
{
    public class CompletedLineView : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;

        private readonly List<Vector3> _positions = new List<Vector3>();

        public void AddPosition(Vector3 position)
        {
            _positions.Add(position);

            _lineRenderer.positionCount = _positions.Count;
            _lineRenderer.SetPositions(_positions.ToArray());
        }

        public void SetAlpha(float alpha)
        {
            var c = _lineRenderer.startColor;
            var color = new Color(c.r, c.g, c.b, alpha);

            _lineRenderer.startColor = _lineRenderer.endColor = color;
        }

        public Tween DoFade(float value, float timeMs)
        {
            var startColor = _lineRenderer.startColor;
            var endColor = new Color(startColor.r, startColor.g, startColor.b, value);

            return _lineRenderer.DOColor(new Color2(startColor, startColor), new Color2(endColor, endColor), timeMs);
        }

        public void Clear()
        {
            _positions.Clear();
            _lineRenderer.positionCount = 0;
        }

        private void OnValidate()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }
    }
}
