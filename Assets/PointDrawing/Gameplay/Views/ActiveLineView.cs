﻿using DG.Tweening;
using UnityEngine;

namespace PointDrawing.Gameplay.Views
{
    public class ActiveLineView : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private float _highlightTimeMs;
        [SerializeField] private Color _startColor;
        [SerializeField] private Color _highlightColor;

        private Tween _tween;

        private void OnEnable()
        {
            _tween?.Kill();

            var halfTimeMs = _highlightTimeMs / 2f;

            _tween = DOTween.Sequence()
                .Append(_lineRenderer.DOColor(new Color2(_startColor, _startColor), new Color2(_highlightColor, _highlightColor), halfTimeMs))
                .Append(_lineRenderer.DOColor(new Color2(_highlightColor, _highlightColor), new Color2(_startColor, _startColor), halfTimeMs))
                .SetLoops(-1);
        }

        private void OnDisable()
        {
            _tween?.Kill();
        }

        public void SetFirstPosition(Vector3 position)
        {
            _lineRenderer.SetPosition(0, position + transform.position);
        }

        public void SetLastPosition(Vector3 position)
        {
            _lineRenderer.SetPosition(1, position + transform.position);
        }

        private void OnValidate()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _startColor = _lineRenderer != null ? _lineRenderer.startColor : Color.white;
        }
    }
}
