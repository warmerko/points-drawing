﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PointDrawing.Gameplay.Views
{
    public class PointView : FadeView, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private TextMeshPro _textField;
        [SerializeField] private CircleCollider2D _collider;
        [SerializeField] private ParticleSystem _particles;

        [SerializeField] private Vector3 _shakeStrength = new Vector3(.5f, .2f);
        [SerializeField] private float _highlightScale = 1.2f;
        [SerializeField] private float _completedScale = 0.8f;
        [SerializeField] private float _nextScale = 1.2f;

        private PointerEventData _pointerEventData;

        public event Action<Vector3> OnDragEvent;
        public event Action OnBeginDragEvent;
        public event Action OnEndDragEvent;
        public CircleCollider2D Collider => _collider;
        public PointerEventData PointerEventData => _pointerEventData;

        public Tween DoIn()
        {
            _tween?.Kill();

            var sequence = DOTween.Sequence()
                .Append(DoFade(1f))
                .Join(transform.DOShakeScale(_animationTimeMs, _shakeStrength));

            _tween = sequence;

            return sequence;
        }

        public Tween SetHighlight(int loops = -1)
        {
            _tween?.Kill();

            var halfTimeMs = _animationTimeMs / 2f;

            _tween = DOTween.Sequence()
                .Append(transform.DOScale(Vector3.one * _highlightScale, halfTimeMs))
                .Append(transform.DOScale(Vector3.one, halfTimeMs))
                .SetLoops(loops);

            return _tween;
        }

        public Tween SetCompleted()
        {
            _tween?.Kill();

            _tween = transform.DOScale(Vector3.one * _completedScale, _animationTimeMs);

            return _tween;
        }

        public Tween SetNext()
        {
            _tween?.Kill();

            _tween = transform.DOScale(Vector3.one * _nextScale, _animationTimeMs);

            return _tween;
        }

        public async UniTask ShowParticlesAsync()
        {
            var token = this.GetCancellationTokenOnDestroy();

            _particles.gameObject.SetActive(true);
            _particles.Play();

            await UniTask.Delay(TimeSpan.FromSeconds(_particles.main.duration), cancellationToken: token);

            _particles.gameObject.SetActive(false);
        }

        public void SetNumber(int number)
        {
            _textField.text = number.ToString();
        }

        public void ResetView()
        {
            _tween?.Kill();
            _tween = null;

            transform.localScale = Vector3.one;
            _particles.gameObject.SetActive(false);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _pointerEventData = eventData;
            OnBeginDragEvent?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _pointerEventData = eventData;
            OnDragEvent?.Invoke(eventData.pointerCurrentRaycast.worldPosition);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _pointerEventData = null;
            OnEndDragEvent?.Invoke();
        }

        protected override void OnValidate()
        {
            _textField = GetComponentInChildren<TextMeshPro>();
            _collider = GetComponent<CircleCollider2D>();
            _particles = GetComponentInChildren<ParticleSystem>(true);

            base.OnValidate();
        }
    }
}
