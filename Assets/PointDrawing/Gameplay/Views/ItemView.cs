﻿using UnityEngine;

namespace PointDrawing.Gameplay.Views
{
    [RequireComponent(typeof(PolygonCollider2D))]
    public class ItemView : FadeView
    {
        [SerializeField] private PolygonCollider2D _collider;

        [SerializeField] private Bounds _bounds;

        public Vector2[] Points => _collider.points;
        public Bounds Bounds => _bounds;

        protected override void OnValidate()
        {
            _collider = GetComponent<PolygonCollider2D>();

            if (_collider == null) return;

            _bounds = new Bounds();

            foreach (var point in _collider.points)
            {
                _bounds.Encapsulate(point);
            }

            base.OnValidate();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1f, 0f, 0f, 0.2f);

            var t = transform;
            var scale = t.localScale;
            var size = new Vector3(_bounds.size.x * scale.x, _bounds.size.y * scale.y, _bounds.size.z * scale.z);
            var center = new Vector3(_bounds.center.x * scale.x, _bounds.center.y * scale.y, _bounds.center.z * scale.z);

            Gizmos.DrawCube(t.position + center, size);
        }
    }
}
