﻿using UnityEngine;

namespace PointDrawing.Gameplay.Views
{
    public class BackgroundView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _board;
        public Vector3 BoardSize => _board.bounds.size;
    }
}
