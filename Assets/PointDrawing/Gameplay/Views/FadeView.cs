﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace PointDrawing.Gameplay.Views
{
    public class FadeView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer[] _spriteRenderers;
        [SerializeField] private TextMeshPro[] _textMeshPros;
        [SerializeField] protected float _animationTimeMs;

        protected Tween _tween;

        public Tween DoFade(float to)
        {
            _tween?.Kill();

            var sequence = DOTween.Sequence();

            if (_spriteRenderers.Length > 0)
            {
                sequence.Append(_spriteRenderers[0].DOFade(to, _animationTimeMs));

                for (var i = 1; i < _spriteRenderers.Length; ++i)
                {
                    sequence.Join(_spriteRenderers[i].DOFade(to, _animationTimeMs));
                }
            }

            if (_textMeshPros.Length > 0)
            {
                foreach (var textMeshPro in _textMeshPros)
                {
                    var color = new Color(textMeshPro.color.r, textMeshPro.color.g, textMeshPro.color.b, to);

                    sequence.Join(DOTween.To(() => textMeshPro.color, c => textMeshPro.color = c, color, _animationTimeMs));
                }
            }

            return _tween = sequence;
        }

        public void SetAlpha(float value)
        {
            foreach (var spriteRenderer in _spriteRenderers)
            {
                var color = spriteRenderer.color;

                spriteRenderer.color = new Color(color.r, color.g, color.b, value);
            }

            foreach (var textMeshPro in _textMeshPros)
            {
                var color = textMeshPro.color;

                textMeshPro.color = new Color(color.r, color.g, color.b, value);
            }
        }

        protected virtual void OnValidate()
        {
            var renderers = new List<SpriteRenderer>(GetComponents<SpriteRenderer>());
            renderers.AddRange(GetComponentsInChildren<SpriteRenderer>());

            var textMeshPros = new List<TextMeshPro>(GetComponents<TextMeshPro>());
            textMeshPros.AddRange(GetComponentsInChildren<TextMeshPro>());

            _spriteRenderers = renderers.ToArray();
            _textMeshPros = textMeshPros.ToArray();
        }
    }
}
