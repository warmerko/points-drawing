﻿using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using PointDrawing.Common.Utils;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Views;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace PointDrawing.Gameplay.Factories
{
    public class ItemsFactory
    {
        private readonly HashSet<AssetReference> _generatedItems;
        private readonly ItemsConfig _itemsConfig;

        protected ItemsFactory(ItemsConfig itemsConfig)
        {
            _itemsConfig = itemsConfig;
            _generatedItems = new HashSet<AssetReference>();
        }

        public UniTask<ItemView[]> CreateRandomAsync(int count)
        {
            var itemsToGenerate = GeneratePseudoRandomItems(count);
            var tasks = new UniTask<ItemView>[count];

            for (var i = 0; i < count; ++i)
            {
                tasks[i] = AddressablesUtils.InstantiateAsync<ItemView>(itemsToGenerate[i]);
            }

            return UniTask.WhenAll(tasks);
        }

        private IList<AssetReference> GeneratePseudoRandomItems(int count)
        {
            var possibleItems = _itemsConfig.Items.Except(_generatedItems).ToList();

            if (possibleItems.Count < count)
            {
                _generatedItems.Clear();

                var addItemsCount = count - possibleItems.Count;
                var additionalItems = _itemsConfig.Items.Except(possibleItems).ToArray();

                for (var i = 0; i < addItemsCount; ++i)
                {
                    possibleItems.Add(additionalItems[i]);
                }
            }

            var items = new List<AssetReference>(count);

            for (var i = 0; i < count; ++i)
            {
                var index = Random.Range(0, possibleItems.Count);
                var item = possibleItems[index];

                items.Add(item);

                possibleItems.Remove(item);

                _generatedItems.Add(item);
            }

            return items;
        }
    }
}
