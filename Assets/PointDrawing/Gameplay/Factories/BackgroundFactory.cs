﻿using Cysharp.Threading.Tasks;
using PointDrawing.Common.Utils;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Views;
using UnityEngine;

namespace PointDrawing.Gameplay.Factories
{
    public class BackgroundFactory
    {
        private readonly BackgroundsConfig _config;

        protected BackgroundFactory(BackgroundsConfig config)
        {
            _config = config;
        }

        public UniTask<BackgroundView> CreateRandomAsync()
        {
            return AddressablesUtils.InstantiateAsync<BackgroundView>(_config.Backgrounds[Random.Range(0, _config.Backgrounds.Length)]);
        }
    }
}
