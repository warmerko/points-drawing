﻿using Cysharp.Threading.Tasks;
using PointDrawing.Common.Utils;
using PointDrawing.Gameplay.Configs;
using PointDrawing.Gameplay.Views;

namespace PointDrawing.Gameplay.Factories
{
    public class PointsFactory
    {
        private readonly ItemsConfig _config;

        protected PointsFactory(ItemsConfig config)
        {
            _config = config;
        }

        public async UniTask<PointView[]> CreateAsync(int count)
        {
            var views = new PointView[count];
            var tasks = new UniTask<PointView>[count];

            for (var i = 0; i < count; ++i)
            {
                tasks[i] = AddressablesUtils.InstantiateAsync<PointView>(_config.Point);
            }

            views = await UniTask.WhenAll(tasks);

            for (var i = 0; i < count; ++i)
            {
                views[i].SetNumber(i + 1);
            }

            return views;
        }
    }
}
