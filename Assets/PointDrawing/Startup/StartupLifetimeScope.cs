using PointDrawing.Common.Views;
using PointDrawing.Gameplay.Configs;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PointDrawing.Startup
{
    public class StartupLifetimeScope : LifetimeScope
    {
        [SerializeField] private AssetsConfig _assetsConfig;
        [SerializeField] private LoadingScreenView _loadingScreenView;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(_assetsConfig);
            builder.RegisterInstance(_loadingScreenView);

            builder.RegisterEntryPoint<Startup>();
        }
    }
}
