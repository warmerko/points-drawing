﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;
using VContainer.Unity;

namespace PointDrawing.Startup
{
    public class Startup : IAsyncStartable
    {
        private readonly LifetimeScope _currentScope;

        protected Startup(LifetimeScope currentScope)
        {
            _currentScope = currentScope;
        }

        public async UniTask StartAsync(CancellationToken _)
        {
            using (LifetimeScope.EnqueueParent(_currentScope))
            {
                await SceneManager.LoadSceneAsync("Gameplay", LoadSceneMode.Additive);
            }
        }
    }
}
