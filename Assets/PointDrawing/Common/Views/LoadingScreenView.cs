﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace PointDrawing.Common.Views
{
    public class LoadingScreenView : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private GameObject _loading;
        [SerializeField] private Button _restartButton;
        [SerializeField] private float _fadeTimeMs;

        public Button RestartButton => _restartButton;

        public void SetLoadingVisible(bool isVisible)
        {
            _loading.SetActive(isVisible);
        }

        public Tween DoFade(float alpha)
        {
            return _canvasGroup.DOFade(alpha, _fadeTimeMs);
        }
    }
}
