﻿using UnityEngine;

namespace PointDrawing.Common.Utils
{
    public static class ScreenUtils
    {
        public static Vector2 GetScreenToWorldSize(Camera camera)
        {
            return camera.ViewportToWorldPoint(Vector2.one) * 2f;
        }
    }
}
