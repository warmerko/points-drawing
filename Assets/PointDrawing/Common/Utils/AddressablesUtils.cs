﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace PointDrawing.Common.Utils
{
    public static class AddressablesUtils
    {
        public static async UniTask<T> LoadAssetAsync<T>(AssetReference reference) where T : Object
        {
            var handle = Addressables.LoadAssetAsync<T>(reference);

            try
            {
                await handle;

                if (handle.Status != AsyncOperationStatus.Succeeded)
                {
                    throw new Exception($"Can't load {reference}");
                }

                return handle.Result;
            }
            finally
            {
                Addressables.Release(handle);
            }
        }

        public static async UniTask<T> InstantiateAsync<T>(AssetReference reference) where T : Component
        {
            var handle = Addressables.InstantiateAsync(reference);

            await handle;

            if (handle.Status != AsyncOperationStatus.Succeeded)
            {
                throw new Exception($"Can't load {reference}");
            }

            var component = handle.Result.GetComponent<T>();

            if (component == null)
            {
                throw new Exception($"Can't get component {typeof(T).FullName}");
            }

            return component;
        }
    }
}
